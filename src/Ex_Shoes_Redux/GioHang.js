import React, { Component } from "react";
import { connect } from "react-redux";
import { DELETE_TO_CART, TANG_GIAM_SL } from "./redux/constants/shoeConstant";

class GioHang extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <img style={{ width: 50 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.tangGiamSoLuong(index, true);
              }}
              className="btn btn-secondary"
            >
              +
            </button>
            <span className="px-2">{item.soLuong}</span>
            <button
              onClick={() => {
                this.props.tangGiamSoLuong(index, false);
              }}
              className="btn btn-secondary"
            >
              -
            </button>
          </td>
          <td>
            <button
              onClick={() => {
                this.props.xoaGioHangIndex(index);
              }}
              className="btn btn-danger"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <div className="text-right ">
          {/* Button trigger modal */}
          <button
            type="button"
            className="btn btn-danger"
            data-toggle="modal"
            data-target="#modelId"
          >
            Giỏ hàng
          </button>
          {/* Modal */}
          <div
            className="modal fade"
            id="modelId"
            tabIndex={-1}
            role="dialog"
            aria-labelledby="modelTitleId"
            aria-hidden="true"
          >
            <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title text-danger  text-center">Giỏ hàng của bạn</h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">x</span>
                  </button>
                </div>
                <div className="modal-body">
                <table className="table text-center">
          <thead>
            <tr>
              <th>id</th>
              <th>Tên</th>
              <th>Giá</th>
              <th>Hình ảnh</th>
              <th>Số lượng</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
          <tfoot>
            <td colSpan={4}></td>
            <td className="">
              Tổng tiền:
            </td>
            <td>{this.props.cart.reduce((tongTien, spGH, index) => {
              return tongTien += spGH.soLuong * spGH.price;
            }, 0)}</td>
          </tfoot>
        </table>
                </div>
                <div className="modal-footer">
                  <button
                    type="button"
                    className="btn btn-secondary"
                    data-dismiss="modal"
                  >
                    Đóng
                  </button>
                  <button type="button" className="btn btn-primary">
                    Thanh toán
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>

     

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.gioHang,
  };
};

const mapDispatchToProps = (disptach) => {
  return {
    xoaGioHangIndex: (index) => {
      disptach({
        type: DELETE_TO_CART,
        payload: index,
      });
    },
    tangGiamSoLuong: (index, tangGiam) => {
      disptach({
        type: TANG_GIAM_SL,
        payload: {
          index,
          tangGiam,
        },
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GioHang);
