import { data_shoes } from "../../data_shoes";
import { ADD_TO_CART, DELETE_TO_CART, TANG_GIAM_SL } from "../constants/shoeConstant";

const initialState = {
    shoes: data_shoes,
    gioHang: [],
};

export let shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
       let index = state.gioHang.findIndex((item) => {
        return item.id == payload.id;
       });
       let cloneGioHang = [...state.gioHang];
       if (index == -1) {
        let newSp = {...payload, soLuong : 1};
        cloneGioHang.push(newSp);
       }
       else {
        cloneGioHang[index].soLuong++;
       }
       state.gioHang = cloneGioHang;
       return {...state};
    }
    case DELETE_TO_CART : {
      let cloneGioHang = [...state.gioHang];
      cloneGioHang.splice(payload, 1);
      state.gioHang = cloneGioHang;
      return {...state};
    }
    case TANG_GIAM_SL: {
      console.log("payloadTangGiam: ",payload.tangGiam);
      let cloneGioHang = [...state.gioHang];
      if (payload.tangGiam == true) {
        cloneGioHang[payload.index].soLuong ++;
      }
      else {
        if (cloneGioHang[payload.index].soLuong > 1) {
          cloneGioHang[payload.index].soLuong --;

        }
      }
      state.gioHang = cloneGioHang;
      return {...state};
    }
  default:
    return state;
  };
};
