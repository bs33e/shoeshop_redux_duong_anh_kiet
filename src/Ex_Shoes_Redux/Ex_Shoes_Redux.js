import React, { Component } from "react";
import { connect } from "react-redux";
import { data_shoes } from "./data_shoes";
import GioHang from "./GioHang";
import ItemShoe from "./ItemShoe";
import { ADD_TO_CART } from "./redux/constants/shoeConstant";

class Ex_Shoes_Redux extends Component {
  state= {
    shoes: data_shoes,
    gioHang: [],
  };

  renderContent = () => {
    return this.props.listShoe.map((item) => {
      return <ItemShoe handleAddToCart={this.props.handleAddToCartRedux} data={item} />;
    });
  };


  // 2 + -1
  handleChangeQuantity = (idShoe, value) => {
    let index = this.state.gioHang.findIndex((shoe) => {
      return shoe.id == idShoe;
    });

    if (index == -1) return;

    let cloneGioHang = [...this.state.gioHang];

    cloneGioHang[index].soLuong = cloneGioHang[index].soLuong + value;

    cloneGioHang[index].soLuong == 0 && cloneGioHang.splice(index, 1);

    this.setState({ gioHang: cloneGioHang });
  };
  render() {
    

    return (
      <div className="container py-5">
      
          <GioHang
            handleChangeQuantity={this.handleChangeQuantity}
            gioHang={this.state.gioHang}
          />
        <h3 className="text-center text-danger">Danh sách sản phẩm</h3>
        <div className="row">{this.renderContent()}</div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    listShoe: state.shoeReducer.shoes,
    cart: state.shoeReducer.gioHang,

  };
};

const mapDispatchToProps =(dispatch) => {
  return {
    handleAddToCartRedux: (shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoe,
      });
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Ex_Shoes_Redux);